# ARX Service

<p align="center">
<a href="https://packagist.org/packages/denagus/arxservice"><img src="https://img.shields.io/packagist/dt/denagus/arxservice" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/denagus/arxservice"><img src="https://img.shields.io/packagist/v/denagus/arxservice" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/denagus/arxservice"><img src="https://img.shields.io/packagist/l/denagus/arxservice" alt="License"></a>
</p>

**ARX Service** merupakan package yang digunakan oleh aplikasi ARX untuk request HTTP antar service dengan fitur authentication dan security access

### Versi yang Didukung
| Version | Laravel Version |
|---- |----|
| > 1.0.0 | >=5.7 |


### Cara Install

- `composer require denagus/arxservice`

    ##### Laravel 

    - Otomatis terdaftar oleh `Laravel Package Discovery`

    ##### Lumen

    - Daftarkan service provider di `bootstrap/app.php`
    	```php
    	$app->register(Denagus\ArxService\ArxServiceProvider::class);
    	```
### Setting Konfigurasi 
- Buat file `config/arxservice.php`
    ```php
    <?php

    return [
    
        /*
        |--------------------------------------------------------------------------
        | Encryption Key
        |--------------------------------------------------------------------------
        |
        | This key is used by the ARX Service and should be set
        | to a random, 32 character string, otherwise these encrypted strings
        | will not be safe. Please do this before deploying an application!
        |
        */
    
        'key' => env('APP_KEY'),
    
        'cipher' => 'AES-256-CBC',
    
        /*
        |--------------------------------------------------------------------------
        | Hash
        |--------------------------------------------------------------------------
        |
        | This key is used by the ARX Service
        |
        */
    
        'hash' => 'sha256',
    
        /*
        |--------------------------------------------------------------------------
        | Expire
        |--------------------------------------------------------------------------
        |
        | The expire time is the number of seconds that the access key should be
        | considered valid. This security feature keeps access keys short-lived so
        | they have less time to be guessed. You may change this as needed.
        |
        */
    
        'expire' => 14400
    
    ];
    ```
- Daftarkan file konfigurasi
    #### Laravel
    - Otomatis terdaftar
    #### Lumen
    - Daftarkan file konfigurasi di `app/bootstrap.php`
    	```php
    	$app->configure('arxservice');
    	```
### Setting Middleware
- Pastikan setiap request antar service menggunakan middleware `service`
    #### Laravel
    - Daftarkan middleware service di `app/Http/Kernel.php`
        ```php
        /**
         * The application's route middleware.
         *
         * These middleware may be assigned to groups or used individually.
         *
         * @var array
         */
    	protected $routeMiddleware = [
            'service'   => \Denagus\ArxService\Middleware\Accesskey::class,
        ];
    	```
    #### Lumen
    - Daftarkan middleware service di `app/bootstrap.php`
        ```php
    	$app->routeMiddleware([
            'service'   => Denagus\ArxService\Middleware\Accesskey::class,
        ]);
    	```
- Contoh penggunaan middleware pada file `controller`
    ```php
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('service');
    }
	```
### Setting Service
1. Buat folder pada path  `app/Libraries/Services`
2. Buat file `UserService.php`
    ```php
    <?php

    namespace App\Libraries\Services;

    use Denagus\ArxService\Service;

    class UserService extends Service
    {
        /**
        * SERVICE_USER_URI
        */
        protected $baseUri = 'SERVICE_USER_URI';
        
        /**
        * Available URL
        */
        private const USER_BY_ID = 'api/v1/user/service/by-user-id';
    }
    ```
3. Buat file `AuthService.php`
    ```php
    <?php

    namespace App\Libraries\Services;

    use Denagus\ArxService\Service;

    class AuthService extends Service
    {
        /**
        * SERVICE_USER_URI
        */
        protected $baseUri = 'SERVICE_AUTH_URI';
    }
    ```
3. Tambahkan `SERVICE_USER_URI` pada .env, dan atur URI dari masing-masing service
    ```php
    SERVICE_USER_URI=http://localhost
    SERVICE_AUTH_URI=http://localhost
    ```

### Cara Penggunaan
* Cara request ke service user / `UserService`
    ```php
    use App\Libraries\Services\UserService;
    use Denagus\ArxService\Exception as ServiceException;
    
    try {
        $userService = UserService::get(UserService::USER_BY_ID . "/1");
        ServiceException::on($userService);
        $user = $userService->data;
    } catch (\Exception $e) {
        $user = null;
    }
    ```
