<?php

namespace Denagus\ArxService;

use Illuminate\Support\ServiceProvider;

class ArxServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/arxservice.php', 'arxservice');
        $this->app->make('Denagus\ArxService\Auth');
        $this->app->make('Denagus\ArxService\Exception');
        $this->app->make('Denagus\ArxService\Service');
        $this->app->make('Denagus\ArxService\Helpers\Security');
        $this->app->make('Denagus\ArxService\Middleware\AccessKey');
    }
}
